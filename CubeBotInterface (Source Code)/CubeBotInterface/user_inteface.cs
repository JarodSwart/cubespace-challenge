using Gtk;
using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace CubeBotInterface{ 
    class MainClass {
        
        static System.Net.Sockets.Socket tx;
        static UdpClient rx;

        // Test whether TLM response is a NACK or ACK
        public static Boolean IsNACKOrACK(byte[] b){
            return b.SequenceEqual(new byte[]{ 0x1f, 0x7f, 0x00, 0x1f, 0xff }) || b.SequenceEqual(new byte[] { 0x1f, 0x7f, 0x01, 0x1f, 0xff });
        }

        // TX function
        public static void SockTX(System.Net.Sockets.Socket sock, byte[] msg) {
            try {
                sock.SendTo(msg, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11001));
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
        // RX function
        public static List<byte> SockRX()
        {
            var msg = new List<byte>();
            try
            {
                IPEndPoint ip = new IPEndPoint(IPAddress.Any, 11000);
                int i = 0;
                byte[] part = MainClass.rx.Receive(ref ip);
                msg.Add(part[0]);
                do
                {
                    i++;
                    part = MainClass.rx.Receive(ref ip);
                    msg.Add(part[0]);
                } while (!(msg[i] == 0xFF && msg[i - 1] == 0x1F));

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            return msg;
        }

        // When called, queries CubeBot's ID to test whether still connected
        public static Boolean TestConnected()
        {
            SockTXTLM(0);
            var b = SockRX();
            if (b == null)
                return false;
            else
                return true;
        }

        // Return ASCII representation of message
        public static string GetString(byte[] b) {
            return System.Text.Encoding.UTF8.GetString(b.Skip(2).Take(b.Length - 4).ToArray());
        }

        // General function for getting the three TLM requests
        public static void SockTXTLM(uint msg_num) {
            // msg_num: 0 -> ID TLM, 1 -> Firewall TLM, 2 -> Email TLM
            if(msg_num >= 0 && msg_num <= 2)
                SockTX(MainClass.tx, new byte[] {0x1F, 0x7F, Convert.ToByte(0x80 + msg_num), 0x1F, 0xFF});
            else
                SockTX(MainClass.tx, new byte[] { 0x1F, 0x7F, 0x80, 0x1F, 0xFF });

        }

        // Get the info to be displayed on the GUI
        public static string GetTLMArr(uint tlm_type)
        {
            SockTXTLM(tlm_type);

            var o = SockRX();
            if (o == null)
                return null;
            else
            {
                if (MainClass.IsNACKOrACK(o.ToArray()))
                    return BitConverter.ToString(o.ToArray()).Replace("-", " ");
                else 
                    return BitConverter.ToString(o.ToArray()).Replace("-", " ") + "\n(" + MainClass.GetString(o.ToArray()) + ")";
            }
        }

        // Toggle connection between GUI and CubeBot
        // Note that the UDP connection is a connectionless protocol - so "disconnecting" 
        // is esentially closing the Socket, and "connecting" is reinitializing it.
        // We can test connection to CubeBot using the TestConnection() function. 
        public static void ToggleTLM(Boolean connected)
        {
            if (!connected)
            {
                MainClass.rx = new UdpClient(11000);
                MainClass.tx = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                MainClass.rx.Client.ReceiveTimeout = 5000;
            }
            else
            {
                MainClass.rx.Close();
                MainClass.tx.Close();
            }
        }

        static void Main (String[] args) {

            // Start the User Interface

            Application.Init();
            MainWindow win = new MainWindow();
            win.Show();
            Application.Run();

            // Test case for if connection has not been made yet, but the interface is closed
            if (MainClass.rx != null && MainClass.tx != null)
                ToggleTLM(true);

        }
    }
}