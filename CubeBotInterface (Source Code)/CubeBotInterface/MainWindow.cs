﻿using System;
using Gtk;
using CubeBotInterface;
using System.Linq;

public partial class MainWindow : Gtk.Window
{

    static Boolean Connected = false;

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    // ID TLM button click action
    protected void show_id_tlm(object sender, EventArgs e)
    {
        if(Connected)
        {
            var buf = MainClass.GetTLMArr(0);
            if (buf != null)
            {
                id_text.Buffer.Text = buf;
            }
            else
            {
                id_text.Buffer.Text = "Error receiving data";
                connected_label.Text = "Disconnected";
                Connected = false;
            }
        }
        else
        {
            id_text.Buffer.Text = "";
        }
    }

    // Firewall TLM button click action
    protected void show_firewall_tlm(object sender, EventArgs e)
    {
        if (Connected)
        {
            var buf = MainClass.GetTLMArr(1);
            if (buf != null)
            {
                firewall_text.Buffer.Text = buf;
            }
            else
            {
                firewall_text.Buffer.Text = "Error receiving data";
                connected_label.Text = "Disconnected";
                Connected = false;
            }
        }
        else
        {
            firewall_text.Buffer.Text = "";
        }


    }

    // Email TLM button click action
    protected void show_email_tlm(object sender, EventArgs e)
    {
        if (Connected)
        {
            var buf = MainClass.GetTLMArr(2);
            if (buf != null)
            {
                email_text.Buffer.Text = buf;
            }
            else
            {
                email_text.Buffer.Text = "Error receiving data";
                connected_label.Text = "Disconnected";
                Connected = false;
            }
        }
        else
        {
            email_text.Buffer.Text = "";
        }
    }

    // Connect button click action
    protected void check_connected(object sender, EventArgs e)
    {

        MainClass.ToggleTLM(Connected);
        if(!Connected && MainClass.TestConnected())
        {
            Connected = true;
            connected_label.Text = "Connected";
        }
        else
        {
            Connected = false;
            connected_label.Text = "Disconnected";
        }
    }
}
