Hello, and welcome to my solution for the CubeSpace challenge! 

In here you will find: 

- My Python solution (cubebot-python/cubebot.py)
- My shellcode solution (cubebot-python/shellcode/shellcode.c)
- An executable for my implementation of the user interface (CubeBotInterface.exe)
- The source code for my user interface (CubeBotInterFace (Source Code)/CubeBotInterace/user_inteface.cs)
- Other supporting files used to run the emulator etc. 


To run my solution: 

- Navigate to cubebot-python/shellcode
- Run: "make clean" and then "make" to generate the necessary shellcode files
- Run emulator.exe (And load the cubebot.bin file)
- Run CubeBotInterface.exe
- Experiment with the interface to confirm that all TLM requests work, and that connection and disconnection to CubeBot is possible
- Run cubebot.py using "python3 cubebot.py" to show initial TM requests and inject the shellcode exploit
- Observe the console output to confirm that all TLM requests are shown in the terminal, and that the secret email address is shown
- Experiment with the user interface to see whether the email address and firewall disable state are correctly shown


Notes on the user interface (UI): 

- The UI allows for connection to the CubeBot emulator, and provides a way to send the three TLM messages and receive the response
- (Initially, I had planned to allow for inejction of the shellcode from the user interface - but I believe that this would be redundant as the Python file will be run anyways)
- Once the UI has been run, note that the initial connection status to CubeBot is "Disconnected"
- The connection status will remain disconnected, and the TLM requests will be disabled until the "Connect To CubeBot" button is clicked, and the status is changed to "Connected"
- If the emulator is not running, or is otherwise unavailable then the connection request will fail after 5 seconds
- Once the UI is connected to the emulator, the TLM messages can be sent and received. If at any point the emulator is switched off, subsequent TLM requests will fail after 5 seconds and will show "Error receiving data". The connnection status will also change to "Disconnected" - and it will remain so until the "Connect To CubeBot" is clicked again and connection is succesfully made


Please feel free to contact me if there are any queries about my solution! I hope to hear from you soon. 

Kind regards, 

Jarod Swart
