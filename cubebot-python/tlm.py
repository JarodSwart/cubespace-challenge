
# TLM pack and return bytearray
def pack(msg):
	packaged = bytearray([0x1f, 0x7f])
	for x in msg:
		if x == 0x1f:
			packaged += bytearray([0x1f, 0x1f])
		else:
			packaged += bytearray([x])
	packaged += bytearray([0x1f, 0xff]) 
	return packaged

# TLM unpack and return bytearray
# In case of any error, return None
def unpack(msg):
	clear = bytearray()
	escape = 0
	start = 0
	end = 0
	for x in msg:
		
		if x == 0x1f and escape == 0:
			escape = 1
		else:
			if escape == 1 and x == 0x7f:
				start = 1
			elif escape == 1 and x == 0xff:
				end = 1
			else:
				clear += bytearray([x])
			escape = 0

	if start == 1 and end == 1:
		return clear
	else:
		return None

